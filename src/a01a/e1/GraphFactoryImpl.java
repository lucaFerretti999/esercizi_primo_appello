package a01a.e1;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import a01a.sol1.Pair;

public class GraphFactoryImpl implements GraphFactory {

	@Override
	public <X> Graph<X> createDirectedChain(List<X> nodes) {
		return new Graph<X>() {
			
			Set<X> nodeSet = new HashSet<X>();

			@Override
			public Set<X> getNodes() {
				nodeSet.addAll(nodes);
				return nodeSet;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				 return (nodes.indexOf(start) == nodes.indexOf(end)-1);
			}

			@Override
			public int getEdgesCount() {
				return (nodes.size()-1); 
						
			}
			
		};
	}

	@Override
	public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> createDirectedCircle(List<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> createFull(Set<X> nodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
		// TODO Auto-generated method stub
		return null;
	}

}
