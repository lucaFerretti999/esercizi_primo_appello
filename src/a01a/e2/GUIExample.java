package a01a.e2;

import javax.swing.*;


import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.awt.*;

public class GUIExample extends JFrame {
	
    private int size;
    private int boat;
    private Map<JButton, Pair<Integer,Integer>> allButtons = new HashMap<JButton, Pair<Integer,Integer>>();
    private Pair<Integer, Integer> pair;
    private int count = 0;
    private int won =0;
    
    public GUIExample(int size, int boat) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        Random row = new Random();
        Random column = new Random();
        
        
        int cols = size; // {1,2,3,4,5}
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        int rowRand = column.nextInt(size);
        int boatLeftBord = column.nextInt(size-boat+1);
        System.out.println("coord " + rowRand + " " + boatLeftBord);
       
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            this.pair = allButtons.get(bt);
            
            if(allButtons.get(bt).getX()==rowRand &&(allButtons.get(bt).getY() - boatLeftBord <=2)) {
            		bt.setText("X");
            		won++;
            		if(won==3) {
            			System.out.println("YOU WON");
            			System.exit(0);
            		}            
            }
            else {			//miss
            	bt.setText("O");
            	if(count==4) {
            		System.out.println("HAI ESAURITO I TENTATIVI");
            		System.exit(0);
            	}
            	else count++;
            }
                      
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
            final JButton jb = new JButton("");
            jb.addActionListener(al);
            panel.add(jb);
            allButtons.put(jb,new Pair<Integer,Integer>(i,j));
        } 
        }
        this.setVisible(true);
    }

        
    public static void main(String[] args){
        new GUIExample(5,3);
    }
        
}
